# If not running interactively, don't do anything:
[ -z "$PS1" ] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" -a -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

#Global
#bin
if [ -e ~/bin ]; then
    PATH=~/bin:"${PATH}"
fi

if [ -x /usr/local/bin/gecho ]; then
    alias echo='/usr/local/bin/gecho'
fi

#functions
function settitle() { echo -ne "\033]0;$@\007"; }
function hl() { egrep --color -ie $@ -e '$'; }
function finf() {
    find `pwd` \( \( -name .git -o -name '*.pyc' \) -prune \) \
    -o \( -type f -print0 \) \
    | xargs -0 grep -in --color "$@" 2>/dev/null;
}

#execs
alias refresh='source ~/.bash_profile'
alias ref='refresh'
alias find='find 2>/dev/null'
alias h='history'
alias cp='cp -v'
alias mv='mv -v'
alias rm='rm -v'
alias wl='wc -l'
alias grep='grep -i --color'
alias greo='grep'
alias gvg='grep -v grep'
alias cx='chmod +x'
alias bim='vim'
alias cim='vim'
alias v='view'
alias ciew='view'
alias biew='view'
alias screen='screen -dRR'
alias tac='tail -r'
# enterprise whitenoise
alias engage='play -q -c2 -n synth whitenoise band -n 100 24 band -n 300 100 gain +20 &'

#cd
alias cd2='cd ../../'
alias cdt='cd ~/tmp'
alias cdd='cd ~/Downloads'

#ls
alias l='ls -1'
alias la='ls -a'
alias ll='ls -l'
alias lla='ls -al'
alias lal='lla'
alias l1a='ls -a1'
alias la1='l1a'
alias lh='ls -lh'
alias lt='ls -lt'
