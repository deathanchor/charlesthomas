syntax on

set nobackup
set noswapfile

set autoindent

set softtabstop=4
set shiftwidth=4
set backspace=2
set expandtab

set textwidth=80

set ignorecase
set smartcase

set incsearch
set hlsearch

set number
set ruler

set background=light
let g:solarized_termtrans = 1
colorscheme solarized

set wildignore+=*.pyc

map <C-x> :nohlsearch<CR>
nmap <C-u> V: s/\v^(\s*)#+ ?/\1/g<CR>:nohls<CR>
vmap <C-u> : s/\v^(\s*)#+ ?/\1/g<CR>:nohls<CR>
nmap <C-c> V: s/\v^(\s*)/\1# /g<CR>:nohls<CR>
vmap <C-c> : s/\v^(\s*)/\1# /g<CR>:nohls<CR>

map <C-z> :s/( \(.\{-}\) )/(\1)/<CR>:nohls<CR>
vmap <C-z> V:s/( \(.\{-}\) )/(\1)/<CR>:nohls<CR>

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

execute pathogen#infect()

highlight clear SignColumn
set printoptions=number:y

set laststatus=2

if hostname() == "surgat.local"
    set t_ts=]1;
    set t_fs=
endif
let &titlestring = expand("%:t")
let &titleold=''
set title
